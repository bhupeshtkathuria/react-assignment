import React from "react";
import MainApp from "./app/MainApp";

const App = () => {
  return <MainApp />;
};

export default App;
