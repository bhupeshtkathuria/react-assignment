import React from "react";
import Header from "./components/Header";
import LeftPannel from "./components/LeftPannel";
import RightPannel from "./components/RightPannel";
import MidPannel from "./components/MidPannel";
import "./MainApp.css";
const MainApp = () => {
  return (
    <div className="main-body">
      <Header />
      <div className="midPart">
        <div className="midpannel">
          <MidPannel />
        </div>
      </div>
      <div className="leftpannel">
        <LeftPannel />
      </div>
      <div className="rightpannel">
        <RightPannel />
      </div>
    </div>
  );
};

export default MainApp;
