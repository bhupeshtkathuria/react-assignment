import React, { useState } from "react";
import "../styles/TopBar.css";
const TopBar = ({ getDetailsIdCallback, activeItem }) => {
  return (
    <div className="headContainer">
      <ul>
        <li onClick={() => getDetailsIdCallback(0)}>
          <a href="#" className={`${activeItem === 0 ? "active" : "dead"}`}>
            details
          </a>
        </li>
        <li onClick={() => getDetailsIdCallback(1)}>
          <a href="#" className={`${activeItem === 1 ? "active" : "dead"}`}>
            delivery
          </a>
        </li>
        <li onClick={() => getDetailsIdCallback(2)}>
          <a href="#" className={`${activeItem === 2 ? "active" : "dead"}`}>
            fit
          </a>
        </li>
        <li onClick={() => getDetailsIdCallback(3)}>
          <a href="#" className={`${activeItem === 3 ? "active" : "dead"}`}>
            share
          </a>
        </li>
      </ul>
    </div>
  );
};

export default TopBar;
