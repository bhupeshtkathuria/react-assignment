import React, { useState } from "react";
import TopBar from "./TopBar";
import "../styles/LeftPannel.css";

const details = [
  {
    id: 0,
    details:
      " The Forte Lurex Linen Viscose Jacket in Mother of Pearl features lunar lavishness by night and by day: a blazer in a linen blend shot with lurex for a shimmering surface that shines like a star in the sky. it has a straight fit with well defined shoulders and a shawl collar culminating in a button and has been flawlessly finished with three jet pockets with a sartorial feel.",
  },
  {
    id: 1,
    details:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
  },
  {
    id: 2,
    details:
      "t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  },
  {
    id: 3,
    details:
      "randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
  },
];

const ItemRenderer = ({ showitem }) => {
  return (
    <div className="details">
      <p>{details[showitem].details}</p>

      <p>
        See the{" "}
        <span>
          <a href="" style={{ color: "#000" }}>
            EDITOR’S NOTE
          </a>
        </span>
      </p>
      <p>
        Learn about the{" "}
        <span>
          <a href="" style={{ color: "#000" }}>
            DESIGNER
          </a>
        </span>
      </p>
    </div>
  );
};
const LeftPannel = () => {
  const [showitem, setShowItem] = useState(0);
  const getDetailsIdCallback = (id) => {
    setShowItem(id);
  };
  return (
    <>
      <TopBar
        getDetailsIdCallback={getDetailsIdCallback}
        activeItem={showitem}
      />
      <ItemRenderer showitem={showitem} />
    </>
  );
};
export default LeftPannel;
