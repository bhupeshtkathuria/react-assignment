import React, { useState } from "react";
import "../styles/Header.css";
import search from "../assets/Search.png";
import bag from "../assets/Bag.png";
import account from "../assets/Account.png";
import NavMenu from "../assets/Nav.png";
const Header = () => {
  const [showNavbar, setShowNavbar] = useState(false);

  const handleShowNavbar = () => {
    setShowNavbar(!showNavbar);
  };

  return (
    <nav className="navbar">
      <div className="container ">
        <div className="left">
          <a href="#" className="logoName">
            MY COMPANY.COM
          </a>
        </div>

        <div className={` mid nav-elements  ${showNavbar && "active"}`}>
          <ul>
            <li>
              <a href="/">the edit</a>
            </li>
            <li>
              <a href="/blog">new arrival</a>
            </li>
            <li>
              <a href="/projects">designer</a>
            </li>
            <li>
              <a href="/about">clothing</a>
            </li>
            <li>
              <a href="/contact">shoes</a>
            </li>
            <li>
              <a href="/contact">bags</a>
            </li>
            <li>
              <a href="/contact">accessories</a>
            </li>
            <li>
              <a href="/contact">jewelry</a>
            </li>
            <li>
              <a href="/contact">beauty</a>
            </li>
            <li>
              <a href="/contact">home</a>
            </li>
          </ul>
        </div>
        <div className="right">
          <img src={search} alt="search" />
          <img src={bag} alt="bag" />
          <div className="HideAccount-icon">
            <img src={account} alt="account" />
          </div>
          <div className="menu-icon" onClick={handleShowNavbar}>
            <img src={NavMenu} alt="NavMenu" />
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
