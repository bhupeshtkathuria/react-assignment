import React, { useState } from "react";
import product1 from "../assets/product1.png";
import product2 from "../assets/product2.png";
import "../styles/RightPannel.css";
import arrow from "../assets/arrow.png";
import line from "../assets/Line.png";

const sizes = [
  {
    id: 0,
    sizeName: "XS",
    available: true,
  },
  {
    id: 1,
    sizeName: "S",
    available: true,
  },
  {
    id: 2,
    sizeName: "M",
    available: false,
  },
  {
    id: 3,
    sizeName: "L",
    available: true,
  },
  {
    id: 4,
    sizeName: "XXL",
    available: true,
  },
];

const RightPannel = () => {
  const [selectedSize, setSelectedSize] = useState(0);
  const handleSizeSelection = (id) => {
    console.log(id);
    setSelectedSize(id);
  };
  return (
    <div className="productDetailCont">
      <p className="productName">JONATHAN SIMKHAI</p>
      <p className="product-desc">Lurex Linen Viscose Jacket in Conchiglia</p>
      <p className="productPrice">$225</p>
      <p>
        <span style={{ fontWeight: 700, fontFamily: "Roboto, sans-serif" }}>
          COLOR
        </span>{" "}
        <span style={{ fontFamily: "Roboto, sans-serif" }}> CONCHIGLIA</span>
      </p>
      <div className="product-images">
        <div className="sampleSelect">
          <img src={product1} alt="product1" />
        </div>
        <img src={product2} alt="product2" />
      </div>
      <div className="product-size">
        <div className="size">
          <p>
            {" "}
            <span
              style={{
                fontWeight: 600,
                fontSize: "13px",
                textTransform: "uppercase",
                fontFamily: "Helvetica Now Text",
              }}
            >
              size
            </span>{" "}
            <span
              style={{
                fontSize: "13px",
                textTransform: "uppercase",
                fontFamily: "Helvetica Now Text",
              }}
            >
              {" "}
              {sizes[selectedSize]?.sizeName}
            </span>
          </p>
        </div>
        <div className="size-guide">
          <p>
            <a
              href="#"
              style={{
                fontWeight: 600,
                color: "#000",
                fontSize: "13px",
                textTransform: "uppercase",
                fontFamily: "Helvetica Now Text",
              }}
            >
              SIZE GUIDE
            </a>
          </p>
        </div>
      </div>
      <div className="sizes">
        {sizes.map((item) => (
          <div
            key={item.id}
            className={`size-box ${
              selectedSize === item.id ? "selected-size" : "nonSelected-size"
            }  ${!item.available ? "dimmBG" : ""}`}
            onClick={() => handleSizeSelection(item.id)}
          >
            <p
              className={`${
                item.available && selectedSize === item.id
                  ? "selectedSizeColor"
                  : "nonSelectedSizeColor"
              }   `}
            >
              {item.sizeName}
            </p>
            {!item.available ? (
              <img
                style={{ alignSelf: "center", position: "absolute" }}
                src={line}
                alt=""
              />
            ) : null}
          </div>
        ))}
      </div>
      <div className="addToBag">
        <p className="addtobagName">
          ADD TO BAG{" "}
          <span>
            {" "}
            <img src={arrow} alt="arrow" />
          </span>{" "}
        </p>
      </div>
      <div className="otherInfo">
        <p className="otherOffers">
          Get 4 interest-free payments of $196.25 with Klarna LEARN MORE
        </p>
        <p className="otherOffers">Speak to a Personal Stylist CHAT NOW</p>
      </div>
    </div>
  );
};

export default RightPannel;
