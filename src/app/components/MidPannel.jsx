import React from "react";
import image1 from "../assets/Products/image1.png";
import image2 from "../assets/Products/image2.png";
import image3 from "../assets/Products/image3.png";
import image4 from "../assets/Products/image4.png";
import image5 from "../assets/Products/image5.png";
import heart from "../assets/heart.png";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import "../styles/MidPannel.css";
import RightPannel from "./RightPannel";
const MidPannel = () => {
  return (
    <div className="midCont">
      <div className="imageContainer">
        <div className="imagesFrame">
          <div>
            <img
              src={heart}
              style={{ position: "absolute", right: 1 }}
              alt="heart"
            />
            <img src={image1} width={"100%"} height={"100%"} alt="image1" />
          </div>
          <img src={image2} width={"100%"} height={"100%"} alt="image2" />
          <img src={image3} width={"100%"} height={"100%"} alt="image3" />
          <img src={image4} width={"100%"} height={"100%"} alt="image4" />
          <img src={image5} width={"100%"} height={"100%"} alt="image5" />
        </div>
      </div>
      <div className="ResTabletView">
        <Carousel className="carousel" showThumbs={false} autoPlay={true}>
          <div>
            <img src={image1} />
          </div>
          <div>
            <img src={image2} />
          </div>
          <div>
            <img src={image3} />
          </div>
          <div>
            <img src={image4} />
          </div>
          <div>
            <img src={image5} />
          </div>
        </Carousel>
        <div
          className=""
          style={{ marginTop: "50px", marginBottom: "50px" }}
        ></div>
        <RightPannel />
      </div>
      <div className="clothTypes">
        <p>
          <a className="clothtype" href="">
            Jonathan Simkhai
          </a>
        </p>
        <p>
          <a className="clothtype" href="">
            Blazers
          </a>
        </p>
        <p>
          <a className="clothtype" href="">
            Viscose
          </a>
        </p>
      </div>
      <div className="EditorNote">
        <p className="note">a note from the editor</p>
        <p className="noteParagraph">
          The Forte Lurex Linen Viscose Jacket in Mother of Pearl features lunar
          lavishness by night and by day: a blazer in a linen blend shot with
          lurex for a shimmering surface that shines like a star in the sky.{" "}
        </p>
        <p className="editorName">
          -By{" "}
          <a href="#" className="editorname">
            MINNA SHIM
          </a>
          , Fashion Editor
        </p>
      </div>
    </div>
  );
};

export default MidPannel;
